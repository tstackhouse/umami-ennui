---
title: Maple Sriracha Turkey Meatballs
date: 2016/7/12 20:46:25
categories:
    - Food
tags:
    - maple
    - turkey
    - sriracha
    - hors d'oeuvre
---

I first saw this recipe over on [Budget Bytes](http://www.budgetbytes.com/) and gave it a try as Beth prepared it. The flavours were solid overall, but the sauce felt like it was lacking a bit of complexity, and her recipe was heavier on the breadcrumbs, which stretched the meat more, but the meatballs turned out a bit doughy. I made a few modifications to my version, swapping the brown sugar for less sweet, but more flavourful Grade B maple syrup, and cutting the breadcrumbs in half to make meatier meatballs, though this did cut down on the yield.

Instead of pingpong/golfball sized, you can also take the same mix and make smaller balls about the size of a quarter or so and they make a fantastic hors d'oeuvre. They hold up incredibly well overtime and are perfect to put in a crock-pot set to warm. Hit the jump to check out the recipe!

<!-- more -->

**Prep Time**  | About 15 minutes

**Cook time**  | About 25 Minutes

**Total Time**  | About 40 minutes

**Yield**  | About 20-25 meatballs


### Ingredients

#### For the meatballs

  * 1 lb ground turkey
  * 1 lg egg
  * 1/4 cup plain breadcrumbs
  * 3/4 inches fresh ginger, peeled and grated finely
  * 1/2 small onion, grated
  * 1/2 tsp soy sauce
  * 1/2 tsp kosher salt
  * 6 grinds black pepper

#### For the sauce

  * 1/2 cup grade B maple syrup
  * 1 tbsp sriracha (or more to taste)
  * 2 tbsp soy sauce
  * 1 tbsp rice vinegar
  * 2 tbsp cornstarch, dissolved in 1/4 cup Water
  * 1 clove garlic, grated

### Instructions

#### For the meatballs

  1. Preheat oven to 350 ºF
  2. Combine all meatball ingredients and mix gently with your hands until well combined
  3. Portion and roll the mixture into ping-pong ball sized meatballs.
  4. Place the meatballs on a foil lined baking sheet and bake for about 25 minutes.

#### For the sauce

  1. Combine all ingredients except cornstarch in a sauce pan and bring to a boil.
  2. Simmer for a few minutes than slowly add cornstarch mixture until the desired thickness is achieved.
  3. Toss the finished meatballs in the sauce until evenly coated and transfer to serving vessel

### Source/Inspiration

  * [Turkey Sriracha Meatballs from Budget Bytes](http://www.budgetbytes.com/2013/10/turkey-sriracha-meatballs/)
