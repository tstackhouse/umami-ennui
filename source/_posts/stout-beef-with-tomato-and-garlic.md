---
title: Stout Beef with Tomato and Garlic
date: 2016/7/13 19:24:18
categories:
    - Food
tags:
    - beef
    - stout
    - beer
    - garlic
    - tomato
    - main course
    - braise
---

{% asset_img 2.jpg Plated up next to some white rice %}

I originally came up with this recipe to use up a bottle of Stone's smoked porter that was gathering dust, but have since fell in love with it and have made it several more times, tweaking the recipe as I've gone along. A word of caution, this may just blow your mind. It's that good.

Make sure you save the cooking liquid, as well as the fat that will render from the beef and bacon, as both will be of use both for this dish and for others too.  I took a version of this to a cookout once and long after the meat was gone, people were using the sauce on their burgers, hot dogs, even just straight up on a bun.  Hit the jump for the recipe, you'll be glad you did.

<!-- more -->

### Ingredients

  * 4 pound (~2kg) chuck roast, brisket, short ribs, or any other braise-worthy cut of beef
  * 6 rashers bacon (any kind)
  * 1 small can (6oz/170g) tomato paste
  * 12-15 cloves garlic, whole
  * 6-10 grinds black pepper
  * 1 tbsp garlic powder
  * Remaining sauce from a prior batch (if available)

#### Marinade

  * 10 cloves of garlic, crushed
  * 10-15 grinds of black pepper
  * 1/4 cup (60ml) salt
  * 1/4 cup (60ml) maple syrup
  * 1/4 cup (60ml) lemon juice or cider vinegar
  * 1 22oz (650 ml) bottle of stout, porter, or other dark beer (I've had good results with Stone's Smoked Porter)
  * Water

### Instructions

#### Marinade

  1. Dissolve the salt in a small amount hot water, than mix with the remaining ingredients except the beer
  2. Place the meat in an airtight container and add the beer, than the salt mixture
  3. Add enough water to make sure the meat is covered and refrigerate for 2 days

#### Preparation

  1. Preheat oven to 275ºF (135ºC).
  2. Combine the remaining ingredients, except the garlic and bacon, in a bowl and mix thoroughly
  3. Remove the meat from the marinade, and dry the surface of the meat with paper towels
  4. Rub with the tomato mixture and place the seasoned meat on a double layer of heavy duct aluminum foil
  5. Drape the rashers of bacon over the beef, than distribute the garlic cloves between them
  6. Seal foil into a the pouch leaving room for steam to escape and place in the oven for 4 to 5 hours.
  7. Gently remove the meat from the pouch, leaving as much liquid in the pouch as possible
  8. Drain the liquid, garlic cloves, and bacon into a container and optionally blend smooth
  9. Serve immediately with white rice or egg noodles and steamed broccoli.

_-or-_

  1. Allow meat and cooking liquid to cool, than refrigerate overnight
  2. From the cooking liquid, remove the solid disc of fat, and reserve
  3. Melt approximately 1-2 tbsp of the fat in a sauté pan and sauté 1 onion, chopped, along with any other vegetables desired such as sliced mushrooms, peppers, or zucchini.
  4. Add several spoonfuls of leftover cooking liquid to the pan and allow to heat, adding dark beer of choice to loosen the sauce if needed
  5. Slice bite-size portions of the chilled meat and toss with sauce to warm
  6. Serve over white rice or egg noodles with steamed broccoli
