---
draft: true
---
# Red Enchilada Sauce

_Red Enchilada Sauce_




**Prep Time:** 5 minutes

**Cook time:** 5 Minutes

**Total Time:** 10 minutes




**Yield:** 2 1/4 cups sauce




**Ingredients:**

  * 2 Tbsp vegetable oil
  * 2 Tbsp flour
  * 2 Tbsp Chili Powder
  * 3/4 tsp garlic powder
  * 1/2 tsp cumin
  * 1/4 tsp smoked paprika
  * 1/4 tsp oregano
  * 2 cups water
  * 3 oz tomato paste (half a small can)
  * 3/4 tsp salt

  * 1/4 tsp fish sauce
  * 1/2 tsp vinegar



**
**

**Instructions:**

  1. Combine oil, flour, and chili powder in a saucier over medium heat, cook for 1 minute after the mixture starts bubbling
  2. Add remaining dry spices and cook another 15 seconds
  3. Add water and tomato paste, whisk thoroughly and allow it to come to the boil, than reduce heat and allow to simmer
  4. Add salt, fish sauce, and vinegar to taste



**
**

**Sources:**

  * <http://www.budgetbytes.com/2012/08/red-enchilada-sauce/>
